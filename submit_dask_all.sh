#!/bin/bash
#SBATCH -A m1759
#SBATCH -C gpu
#SBATCH -c 40
#SBATCH -N 1
#SBATCH -G 4
#SBATCH -t 3:00:00
#SBATCH --mail-user=lastephey@lbl.gov
#SBATCH --mail-type=all


#source our custom env which contains papermill
#to build:
#module load python
#conda create -n papermill -c conda-forge papermill -y

module load python
source activate papermill

#--how should be either monthly, quarterly, yearly, or all
#srun -u -N 1 -c 40 -G 4 python -u run_papermill_all.py --split='monthly'

#srun -u -N 1 -c 40 -G 4 python -u run_papermill_all.py --split='quarterly'

#srun -u -N 1 -c 40 -G 4 python -u run_papermill_all.py --split='yearly'

srun -u -N 1 -c 40 -G 4 python -u run_papermill_all.py --split='all'

conda deactivate


