# MODS Python data analysis

Here we'll put public examples to demonstrate
how we perform our analysis of the Python data collected
via the MODS project.

## Batch script

`submit_dask_all.sh`

You can use this batch script to submit a Papermill-wrapped
Dask job.

## Papermill script

`run_papermill_all.py`

This Papermill script parses input and loops through the
requested types of processing. It generates and saves an
output notebook for each run.

## Jupyter notebook with Dask for GPU processing

`save-dask-all.ipynb`

Our papermill script will run this Jupyter notebook with
the user-specified choice of parameters.
This notebook starts and runs our Dask
cluster for GPU processing. It uses dask-cudf heavily,
along with some cuDF and CuPy. It reads and saves files
in Parquet format.

## Jupyter notebook rendered into a dashboard by Voila

`public-dashboard.ipynb`

This Jupyter notebook reads our data, plots it, and Voila
is used to render this notebook as an interactive web
dashboard.
