import argparse
import papermill as pm

def parse_arguments():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--split', '-s',
                        help='how to split analysis, specify yearly, quarterly, or monthly')
    args = parser.parse_args()
    return args

args = parse_arguments()
split = args.split

if split == 'monthly':

    data_dict = dict()
    #year is key, month is value
    data_dict['2020'] = ['08','09','10','11','12']
    data_dict['2021'] = ['01','02','03','04','05','06','07'] 
    
    for key,values in data_dict.items():
        year = key
        for value in values:
            month = value
            quarter = None
            print("split", split)
            print("year", year)
            print("quarter", quarter)
            print("month", month)
            pm.execute_notebook(input_path='save-dask-all.ipynb',
                      output_path='.../python-mods/notebooks/save-dask-{}-{}-{}.ipynb'.format(split, year, month),
                      parameters={'split': split, 'year': year, 'quarter': quarter, 'month': month})

elif args.split == 'quarterly':

    data_dict = dict()
    #year is key, month is value
    data_dict['2020'] = ['Q3','Q4']
    data_dict['2021'] = ['Q1','Q2','Q3']
    
    for key,values in data_dict.items():
        year = key
        for value in values:
            quarter = value
            month = None
            print("split", split)
            print("year", year)
            print("quarter", quarter)
            print("month", month)
            pm.execute_notebook(input_path='save-dask-all.ipynb',
                      output_path='.../python-mods/notebooks/save-dask-{}-{}-{}.ipynb'.format(split, year, quarter),
                      parameters={'split': split, 'year': year, 'quarter': quarter, 'month': month})
elif args.split == 'yearly':

    years = ['2020','2021']
    
    for year in years:
        month = None
        quarter = None
        print("split", split)
        print("year", year)
        print("quarter", quarter)
        print("month", month)
        pm.execute_notebook(input_path='save-dask-all.ipynb',
                  output_path='.../python-mods/notebooks/save-dask-{}-{}.ipynb'.format(split, year),
                  parameters={'split': split, 'year': year, 'quarter': quarter, 'month': month})

else: #all

    split = args.split
    year = None
    quarter = None
    month = None

    print("split", split)
    print("year", year)
    print("quarter", quarter)
    print("month", month)

    pm.execute_notebook(input_path='save-dask-all.ipynb',
                  output_path='.../python-mods/notebooks/save-dask-{}.ipynb'.format(split),
                  parameters={'split': split, 'year': year, 'quarter': quarter, 'month': month})



